<?php

use App\Http\Controllers\FlowerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SocialController;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckRole;

Route::get('/', [FlowerController::class, 'load_main'])->name("load_main");


Route::get('/social', [SocialController::class, 'load_social'])->name("load_social");

Route::get('/social/filter/{flower}', [SocialController::class, 'flower_filter'])->name("flower_filter");
Route::post('/social/filter', [SocialController::class, 'flower_filter_dropdown'])->name("flower_filter_dropdown");


//1a vez que carga la vista
Route::get('/flower/form', function() {
    return view('flowerCreation', [
        'datos' => null,
        'error' => null
    ]);
  })->middleware([\App\Http\Middleware\NotLoginMiddleware::class]);


//crear la flor
Route::post('/flower/create', [FlowerController::class, 'create_flower'])->name("create_flower");
//creada, muestra esto
Route::get('/flower/done', [FlowerController::class, 'done_flower'])->name("done_flower");




//Crear post
Route::get('/blog/form', [SocialController::class, 'show_post_form'])->name("show_post_form");

Route::post('/blog/upload', [SocialController::class, 'uploadPost'])->name("uploadPost");
//delete post
Route::get('/blog/delete/{id}', [SocialController::class, 'deletePost'])->name("deletePost");

// BLOGS
Route::get('/flower/{flower}', [FlowerController::class, 'send_flower'])->name("flower");

Route::get('/suerte', [FlowerController::class, 'random_Flower'])->name("random_flower");

Route::post('/search', [FlowerController::class, 'search_flower'])->name("search_flower");





//LOGIN/REGISTER

Route::get('/login', function () {
    return view('userRelated/login');
})->name('login')->middleware([\App\Http\Middleware\LoginMiddleware::class]);

Route::post('/login_auth', [UserController::class, 'login_auth'])->name("login_auth");


Route::get('/register', function () {
    return view('userRelated/register');
})->name('register')->middleware([\App\Http\Middleware\LoginMiddleware::class]);

Route::post('/register_auth', [UserController::class, 'register_auth'])->name("register_auth");
Route::get('/logout', [UserController::class, 'logout'])->name("logout")->middleware([\App\Http\Middleware\NotLoginMiddleware::class]);



Route::get('/profile/{user}', [UserController::class, 'profile'])->name("profile");

Route::get('/profile/{user}/edit', [UserController::class, 'editProfile'])->middleware([\App\Http\Middleware\LoginMiddleware::class])->name("editProfile");
Route::post('/profile/update', [UserController::class, 'updateProfile'])->name("updateProfile");

