@extends('layout.app')

@section('content')
<div class="my-4 p-4">

    <div class="d-flex flex-column align-items-center">
        <img src="{{$user->profile_image}}" alt="" class="h-25 w-25 mx-auto">
        <div>{{$user->username}}</div>
    </div>

    <div class="d-flex flex-column align-items-center justify-content-center mt-5">Numero de posts creados: {{$postUser}}</div>

    <!-- si el user esta login y es el mismo que saque el boton de editar -->
    @if(Session::get('isLoged') && Session::get('userLogged')==$user->username)
    <a href="{{ route('editProfile', ['user' => Session::get('userLogged')]) }}" class="btn btn-success d-block mx-auto">Editar perfil</a>
    @endif
</div>
@endsection