@extends('layout.app')

@section('content')
<div>
    <div class="h1">Welcome back!</div>
    <div class="h4">Fill your credentials to login</div>
</div>

@if(Session::get('wrongEmail'))
<div class="h2 text-danger"> {{ Session::get('wrongEmail'); }}
</div>
@endif


<form action="{{ route('login_auth')}}" method="post" class="">
    @csrf
    <div class="form-group mb-4">
        <label for="" class="label_inputs">Email:</label>
        <!-- <input type="text" name="email_login" id="" class="form-control input_style" required> -->
        <input type="email" name="email" id="" class="form-control input_style" required>
    </div>

    <div class="form-group mb-4">
        <label for="password" class="label_inputs">Password:</label>
        <div class="input-group">
            <input type="password" name="password" id="password" class="form-control input_style rounded" style="z-index: 0;" required>
            <button type="button" id="show-password-btn" class="btn position-absolute mt-41 end-0 top-50 translate-middle-y" style="z-index: 1;">
                <i class="fas fa-eye"></i>
            </button>
        </div>
    </div>



    <div class="d-flex justify-content-center align-items-center flex-column text-center mb-3">
        <input type="submit" value="Login" class="btn buttons"> <br>
        <a href="{{route('register')}}" class="link">Crear una cuenta</a>
    </div>
</form>

<!-- script para revelar y ocultar passwd -->
<script>
    const passwordInput = document.getElementById("password");
    const showPasswordBtn = document.getElementById("show-password-btn");

    showPasswordBtn.addEventListener("click", function() {
        if (passwordInput.type === "password") {
            passwordInput.type = "text";
        } else {
            passwordInput.type = "password";
        }
    });
</script>
@endsection