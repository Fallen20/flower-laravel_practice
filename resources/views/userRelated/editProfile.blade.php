@extends('layout.app')

@section('content')
<div>Edita tu perfil aqui</div>

<form id="my-form" action="{{route('updateProfile')}}" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group d-flex justify-content-center flex-column align-items-center">
        <label for="imageUpload" class="label_inputs mb-2">Imagen de perfil</label>
        <div class="form-group d-flex justify-content-center">
            <label for="imageUpload" style="cursor:pointer">
            <img id="uploaded-image" src="{{ $user->profile_image}}" alt="Image Placeholder" style="height: 250px; max-width:300px;">
            </label>
            <input id="imageUpload" type="file" name="image" class="form-control-file" style="display: none;" onchange="handleImageUpload()">
            <input type="hidden" name="prev_image" value="">
        </div>
    </div>

    

    <div class="form-group mb-4">
        <label for="" class="label_inputs">Cambiar email:</label>
        <input type="email" name="email" id="" class="form-control input_style"  value="{{$user->email}}"> 
    </div>
    <div class="form-group mb-4">
        <label for="" class="label_inputs">Cambiar username:</label>
        <input type="text" name="username" id="" class="form-control input_style" value="{{$user->username}}">
    </div>
    <div class="form-group mb-4">
        <label for="" class="label_inputs">Cambiar password:</label>
        <input type="password" name="password" id="" class="form-control input_style" >
    </div>
    <div class="form-group mb-4">
        <label for="" class="label_inputs">Repetir password:</label>
        <input type="password" name="password_repeat" id="" class="form-control input_style" >
    </div>

    <input type="submit" value="Cambiar datos">

</form>

<script>
   function handleImageUpload() {
        // Get the selected file
        const fileInput = document.getElementById('imageUpload');
        const file = fileInput.files[0];

        // Create a FileReader object to read the file data
        const reader = new FileReader();

        // Define a callback function to be called when the file data is loaded
        reader.onload = function(event) {

            // Create a new image element with the uploaded image data
            const img = document.createElement('img');

            //comprobar que es una imagen
            var mime = event.target.result.split(";")[0].split(":")[event.target.result.split(";")[0].split(":")
                .length - 1];
            let image_extensions = ["jpg", "jpeg", "png", "gif", "bmp", "webp"];

            if (mime.split("/")[0] == 'image' && image_extensions.includes(mime.split("/")[1])) {
                img.src = event.target.result;
                //tamaño
                img.style.maxWidth = "300px";
                img.style.height = "200px";

                // Replace the old image element with the new one
                const oldImg = document.getElementById('uploaded-image');
                oldImg.src = img.src;

                // Add the uploaded image data to the form data
                const formData = new FormData(document.getElementById('my-form'));
                formData.append('image', file);

                // Submit the form
                const xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route("uploadPost") }}');
                xhr.send(formData);
            }
        }

        // Read the file data as a data URL
        reader.readAsDataURL(file);
    }
</script>
@endsection