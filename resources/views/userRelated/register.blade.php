@extends('layout.app')

@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">

@if(Session::get('wrongRegister'))
<div class="h2 text-danger"> {{ Session::get('wrongRegister'); }}
</div>
@endif

<form action="{{ route('register_auth')}}" method="post" class="mt-3">
    @csrf
    <div class="form-group mb-4">
        <label for="" class="label_inputs">Email:</label>
        <input type="email" name="email" id="" class="form-control input_style" required>
    </div>

    <div class="form-group mb-4">
        <label for="" class="label_inputs">Username:</label>
        <input type="text" name="username" id="" class="form-control input_style" required>
    </div>

    <div class="form-group mb-4">
        <label for="password" class="label_inputs">Password:</label>
        <div class="input-group">
            <input type="password" name="pass_login" id="password" class="form-control input_style rounded" style="z-index: 0;" required>
            <button type="button" id="show-password-btn" class="btn position-absolute mt-41 end-0 top-50 translate-middle-y" style="z-index: 1;">
                <i class="fas fa-eye" id="eyePass"></i>
            </button>
        </div>
    </div>

    <div class="form-group mb-4">
        <label for="password" class="label_inputs">Repeat Password:</label>
        <div class="input-group">
            <input type="password" name="pass_login_Repeat" id="password_Repeat" class="form-control input_style rounded" style="z-index: 0;" required>
            <button type="button" id="show-password-btn_Repeat" class="btn position-absolute mt-41 end-0 top-50 translate-middle-y" style="z-index: 1;">
                <i class="fas fa-eye" id="eyePass_Repeat"></i>
            </button>
        </div>
    </div>

    <input type="submit" value="Create account" class="btn btn-success"> <br>


</form>


<script>
    const passwordInput = document.getElementById("password");
    const showPasswordBtn = document.getElementById("show-password-btn");

    //repeat
    const passwordInput_Repeat = document.getElementById("password_Repeat");
    const showPasswordBtn_Repeat = document.getElementById("show-password-btn_Repeat");

    //iconos
    const eyePass = document.getElementById("eyePass");
    const eyePass_Repeat = document.getElementById("eyePass_Repeat");


    //Cambiar iconos+visibilidad
    showPasswordBtn.addEventListener("click", function() {
        if (passwordInput.type === "password") {
            passwordInput.type = "text"; //muestra el texto
            //cambia el ojo
            eyePass.classList.remove("fa-eye"); //eliminar la clase del ojo abierto
            eyePass.classList.add("fa-eye-slash"); //añadir ojo cerrado
        } else {
            passwordInput.type = "password";
            //cambia el ojo
            eyePass.classList.remove('fa-eye-slash'); //eliminar ojo cerrado
            eyePass.classList.add('fa-eye'); //add  la clase del ojo abierto
        }
    });

    showPasswordBtn_Repeat.addEventListener("click", function() {
        if (passwordInput_Repeat.type === "password") {
            passwordInput_Repeat.type = "text";
            //cambia el ojo
            eyePass_Repeat.classList.remove("fa-eye"); //eliminar la clase del ojo abierto
            eyePass_Repeat.classList.add("fa-eye-slash"); //añadir ojo cerrado
        } else {
            passwordInput_Repeat.type = "password";

            //cambia el ojo
            eyePass_Repeat.classList.remove('fa-eye-slash'); //eliminar ojo cerrado
            eyePass_Repeat.classList.add('fa-eye'); //add  la clase del ojo abierto
        }
    });
</script>

@endsection