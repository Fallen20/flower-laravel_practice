@extends('layout.app')

@section('content')
<div class=" p-2">
    <div class="row">
        <!-- col1 -->
        <div class="col-3">
            <div style="height: 200px;">
                <div>Buscador</div>
                <form method="post" role="search" action="{{ route('search_flower') }}">
                    @csrf
                    <input class="InputBuscador" name="search" type="search" placeholder="Search" aria-label="Search" />
                    <input type="submit" class="buscadorBotton" value="🔎"></a>
                </form>


            </div>
            <div>
                <div>
                    Articulos trending
                </div>
                <div>
                    <div class="d-flex flex-column">
                        @foreach($floresArray as $flor)
                        <a href="{{ route('flower', ['flower' => $flor->name]) }}" class="mb-2">{{$flor->name}}</a>

                        @endforeach
                        <a href="/suerte">Aleatorio</a>
                    </div>
                </div>

                <div class="mt-5">
                    <a class="btn buscadorBotton" href="/flower/form">Añadir flor</a>
                </div>
            </div>
        </div>
        <!-- fin Col1 -->
        <div class="col-9">

            @yield('flor')
        </div>
    </div>
</div>

@endsection