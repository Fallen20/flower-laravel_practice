@extends('layout.app')

@section('content')
<form method="post" role="search" action="{{ route('search_flower') }}">
    @csrf
    <input class="InputBuscador" name="search" type="search" placeholder="Search" aria-label="Search" />
    <input type="submit" class="buscadorBotton" value="🔎"></a>
</form>

<div class="d-flex flex-column">
    <div>Sacando los resultados de "<em>{{$busqueda}}</em>"</div>

    <!-- si no está vacio (null) y HAY resultados -->
    @if($flores!=null && count($flores)>0)
    <div class="d-flex col-9 mx-auto justify-content-evenly flex-wrap">

        @foreach($flores as $flor)

        <div class="card me-1 mb-1" style="width: 15rem;">
            <a href="{{ route('flower', ['flower' =>$flor->name ]) }}" class="tituloFlorSearch text-center">
                <img src="{{$flor->img}}" class="card-img-top imgSearch">
                <div class="card-body">
                    <h5 class="card-title ">{{$flor->name}}</h5>
                </div>
            </a>
        </div>


        @endforeach
    </div>
    @else
    <div>No hay resultados</div>
    @endif

</div>

@endsection