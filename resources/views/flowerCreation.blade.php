@extends('layout.app')

@section('content')

<div class="h1 text-center my-4">Rellena los siguientes campos para crear una flor</div>
<div class="h4 text-center text-danger">
    @if($error!=null)
    <div>{{$error}}</div>
    @endif
</div>
<form method="post" role="search" action="{{ route('create_flower') }}">
    @csrf

    <!-- imagen -->

    <div class="form-group mb-3 d-flex flex-column">
        <img id="imgPlace" name="imgPlace" src="https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png" alt="Image Placeholder" class="img-fluid mx-auto" style="height: 250px; max-width:300px;">
        <br>
        <div class="d-flex">

            <input type="text" name="imgLink" id="imgLink" class="form-control input_style" required placeholder="Introduce el link de la imagen" value="{{ $datos ? $datos->imgLink : '' }}">
            <input type="hidden" name="imgSrc" id="imgSrc" value="{{ $datos ? $datos->imgLink :'https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png' }}">


            <input type="button" class="buscadorBotton" value="Load" onclick="checkImage()"></a>
        </div>
    </div>

    <!-- nombre -->
    <div class="form-group mb-3">
        <label for="" class="label_inputs mb-2">Nombre de la flor</label>
        <input type="text" name="name" class="form-control input_style" required value="{{ $datos ? $datos->name :'' }}">
    </div>

    <!-- desc -->
    <div class="form-group mb-3">
        <label for="" class="label_inputs mb-2">Descripción</label>
        <input type="text" name="desc" class="form-control input_style" required value="{{ $datos ? $datos->desc :'' }}">
    </div>

    <!-- care -->
    <div class="form-group mb-3">
        <label for="" class="label_inputs mb-2">Añade los cuidados necesarios</label>
        <input type="text" name="care" class="form-control input_style" required value="{{ $datos ? $datos->care :'' }}">
    </div>
    <input type="submit" class="buscadorBotton" value="Enviar"></a>
</form>



<script>
    var loadingImg = "https://media1.giphy.com/media/3oEjI6SIIHBdRxXI40/200w.gif?cid=6c09b9529h62rz5xtc4rqlkosmeo02imlsi7181gex4w7w66&ep=v1_gifs_search&rid=200w.gif&ct=g";
    var errorImg = "https://img.freepik.com/psd-gratis/simbolo-x-aislado_23-2150500369.jpg";

    function checkImage() {

        //recuperar el input
        var imgInput = document.getElementById('imgLink').value;

        //recuperar el img
        var imgPlace = document.getElementById('imgPlace');
        //input oculto que enviamos
        var imgSrc = document.getElementById("imgSrc");

        //poner una imagen de carga mientras se hace la promesa
        imgPlace.src = loadingImg;
        imgSrc.value = loadingImg;


        //comprobar que el link en el input es una imagen
        isImageURL(imgInput).then(function(result) {
            //es una imagen
            imgPlace.src = imgInput;
            imgSrc.value = imgInput;
        }).catch(function(error) {
            //no es una imagen
            imgPlace.src = errorImg;
            imgSrc.value = errorImg;
        });
    }

    function isImageURL(url) {

        return new Promise((resolve, reject) => {
            var img = new Image(); //se crea un obj image para ver si es valido como imagen

            img.onload = function() {
                resolve(true); // ES una imagen
            };
            img.onerror = function() {
                reject(false); // NO es una imagen
            };
            img.src = url; //sin esto no funciona
        });
    }
</script>
@endsection