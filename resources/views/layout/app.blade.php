<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('cssPropios/cssPropio.css') }}">

</head>

<body>
    <div class="container-fluid ps-0 pe-0">
        <div class=" headerMain">
            <h1 class="mb-3 floraMas">FloraMÁS</h1>

            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-3 d-flex justify-content-evenly">
                        <a href="/" class="linkHeader">Acceso al blog</a>
                        <a href="/social" class="linkHeader">Redes sociales</a>
                    </div>

                    <div class="col-md-9 d-flex justify-content-end pe-5">
                        @if(!Session::get('isLoged'))
                        <a href="/login" class="linkHeader">Log in</a>
                        @else
                        <a href="{{ route('profile', ['user' => Session::get('userLogged')]) }}" class="linkHeader">{{ Session::get('userLogged')}}</a>
                        <a href="/logout" class="linkHeader">Log out</a>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="container">
        @yield('content')
    </div>
</body>

</html>