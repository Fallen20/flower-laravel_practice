@extends('..main')

@section('flor')


@if($flor!=null)
    <div class="tituloFlor">
        {{ $flor->name }}
    </div>
    <div class="d-flex justify-content-center">
        <img src="{{ $flor->img }}" alt="" class="imgBlog">
    </div>

    <div>
        {{ $flor->desc }}
@endif


    @endsection