@extends('layout.app')

@section('content')

@if($datos!=null)
<div class="h1 text-center my-4">Gracias por contribuir con {{ $datos->name  }}</div>
<img src="{{$datos->imgSrc}}" alt="">
<div>Puedes ver la publicación en <a href="{{ route('flower', ['flower' => $datos->name]) }}" class="mb-2">Este link</a>
</div>
@else
<div class="h1 text-center text-danger">No tienes permisos para ver esta página</div>
@endif
@endsection