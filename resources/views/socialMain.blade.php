@extends('layout.app')

@section('content')

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<link rel="stylesheet" href="{{ asset('cssPropios/blogCss.css') }}">




<div class=" p-2">
    <div class="row">
        <!-- col1 -->

        <div class="col-3">

            @if(Session::get('isLoged'))

            <div class="mt-3 d-flex justify-content-center">
                <a href="/blog/form" class="btn btn-light">
                    Crear tu post
                </a>
            </div>
            @endif

            <hr>
            <div class="mt-3">Filtros</div>
            <div class="container">
                <div class="row">


                    <p id="textoDesplegable" class="fw-bold" style="cursor: pointer;">Desplegable de filtros</p>
                    <form id="my-form" action="{{ route('flower_filter_dropdown') }}" method="post">
                        @CSRF
                        <div id="checkbox-list">

                            <!-- esto ha de ser un bucle de todas las flores -->
                            @foreach($flores as $flor)
                            <div class="form-check">
                                @if(gettype($selectedFlower)=='array')

                                <input name="arraySeleccion[]" class="form-check-input" type="checkbox" value="{{$flor->name}}" {{ in_array($flor->name, $selectedFlower) ? 'checked' : '' }} id="checkbox{{$loop->index}}">
                                <label class="form-check-label" for="checkbox1">{{$flor->name}}</label>

                                @else
                                <input name="arraySeleccion[]" name="arraySeleccion[]" class="form-check-input" type="checkbox" value="{{$flor->name}}" {{ $selectedFlower==$flor->name ? 'checked' : '' }} id="checkbox1">
                                <label class="form-check-label" for="checkbox1">{{$flor->name}}</label>
                                @endif
                            </div>
                            @endforeach

                            <input type="submit" value="Aplicar">
                        </div>
                    </form>

                </div>
            </div>
        </div>


        <!-- fin Col1 -->
        <div class="col-9">



            @if($posts!=null)
            @foreach($posts as $post)
            <!-- esto es un for -->
            <div class="container-fluid">

                <div class="row my-5">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="flex-start">
                            <!-- esto lleva al user -->
                            <a href="{{ route('profile', ['user' => $post->userName]) }}" class="d-flex align-items-center text-decoration-none text-success">
                                <img src="{{ $post->userPhoto }}" alt="" class="userImg me-4">
                                <div class="userName">{{$post->userName}}</div>
                            </a>
                        </div>
                        @if(Session::get('userLogged')==$post->userName)
                        <div class="flex-end">
                            <a href="{{ route('deletePost', ['id' => $post->id]) }}" class="btn btn-danger">Borrar</a>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <img src="{{ $post->img }}" alt="" class="imgPost img-fluid w-50 h-50 mx-auto">
                </div>
                <div class="row descPost">
                    {{ $post->description }}
                </div>
                <div class="d-flex justify-content-between">
                    <div class="flex-start tagFlor">
                        <a href="{{ route('flower_filter', ['flower' => $post->flower]) }}" class="textFlor">{{ $post->flower }}</a>
                    </div>
                    <div class="flex-end datePost">{{ $post->created_at }}</div>
                </div>
                <hr>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>

<script>
    const textoDesplegable = document.getElementById('textoDesplegable');
    const checkboxList = document.getElementById('checkbox-list');


    textoDesplegable.addEventListener('click', function() {

        // darle la clase para la animacion. Si la tiene es que ya ha hecho la animacion y se ha clickado de nuevo 
        if (checkboxList.classList.contains('show')) {
            checkboxList.classList.remove('show');
        } else {
            //has clickado pero no ha hecho la animación, añádela
            checkboxList.classList.add('show');
        }


        //tamaño de la animacion. Para que sepa cuanto tiene que hacer la animacion
        if (checkboxList.classList.contains('show')) {
            checkboxList.style.maxHeight = checkboxList.scrollHeight + 'px';
        } else {
            checkboxList.style.maxHeight = '0';
        }
    });
</script>
@endsection