@extends('layout.app')

@section('content')
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>



<form id="my-form" action="{{route('uploadPost')}}" method="POST" enctype="multipart/form-data">
    @csrf

    <!-- imagen -->

    <div class="form-group d-flex justify-content-center flex-column align-items-center">
        <label for="imageUpload" class="label_inputs mb-2">Imagen del post</label>
        <div class="form-group d-flex justify-content-center">
            <label for="imageUpload" style="cursor:pointer">
                <img id="uploaded-image" src="https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png" style="height: 250px; max-width:300px;">
            </label>
            <input id="imageUpload" type="file" name="image" class="form-control-file" style="display: none;" onchange="handleImageUpload()">
            <input type="hidden" name="prev_image" value="">
        </div>
    </div>

    <div class="form-group mb-5">
        <label for="" class="label_inputs mb-2">Descripción</label>
        <textarea type="text" name="desc" class="form-control input_style" rows="10"></textarea>
    </div>

    <div class="form-group mb-5 align-center">

        <label for="" class="label_inputs mb-2"> Elige la flor que el post corresponde</label>
        <div class="btn-group">
            <button type="button" name="" id="selectedValue" class="btn btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                Select a value
            </button>
            <input type="text" name="selectedValue_hidden" id="selectedValue_hidden" hidden/>

            <ul id="dropdown" class="dropdown-menu">
                <!-- foreach con las flores -->
                @foreach($flores as $flor)
                <li class="dropdown-item p-2">{{$flor->name}}</li>
                @endforeach
            </ul>
        </div>

    </div>

    <input type="submit" class="btn btn-" value="Subir post">
</form>




<script>
    // como no es un select, se ha de iterar con todos y hacer que si se ha clickado uno saque la alarme
    document.querySelectorAll("#dropdown li").forEach(function(element) {
        element.onclick = function(e) {

            //cambiar el valor del boton por esto
            document.getElementById('selectedValue').innerHTML = this.innerText;
            document.getElementById('selectedValue_hidden').value = this.innerText;
            document.getElementById('selectedValue_hidden').innerHTML = this.innerText;
            console.log(document.getElementById('selectedValue_hidden').value);
        }
    });




    // que la imagen cambie cuando seleccionas una
    function handleImageUpload() {
        // Get the selected file
        const fileInput = document.getElementById('imageUpload');
        const file = fileInput.files[0];

        // Create a FileReader object to read the file data
        const reader = new FileReader();

        // Define a callback function to be called when the file data is loaded
        reader.onload = function(event) {

            // Create a new image element with the uploaded image data
            const img = document.createElement('img');

            //comprobar que es una imagen
            var mime = event.target.result.split(";")[0].split(":")[event.target.result.split(";")[0].split(":")
                .length - 1];
            let image_extensions = ["jpg", "jpeg", "png", "gif", "bmp", "webp"];

            if (mime.split("/")[0] == 'image' && image_extensions.includes(mime.split("/")[1])) {
                img.src = event.target.result;
                //tamaño
                img.style.maxWidth = "300px";
                img.style.height = "200px";

                // Replace the old image element with the new one
                const oldImg = document.getElementById('uploaded-image');
                oldImg.src = img.src;

                // Add the uploaded image data to the form data
                const formData = new FormData(document.getElementById('my-form'));
                formData.append('image', file);

                // Submit the form
                const xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route("uploadPost") }}');
                xhr.send(formData);
            }
        }

        // Read the file data as a data URL
        reader.readAsDataURL(file);
    }
</script>

@endsection