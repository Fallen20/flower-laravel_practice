<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class flowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('flowers')->insert([
            // le dices como se llama la columna y el valor
            'name' => 'Rosa',
            'img' => 'https://www.mundoeterno.es/505-large_default/rosa-eterna-azul-oscuro-35cm.jpg',
            'desc' => 'El género Rosa está compuesto por un conocido grupo de arbustos generalmente espinosos y floridos representantes principales de la familia de las rosáceas. Se denomina rosa a la flor de los miembros de este género y rosal a la planta.',
            'care' => 'Lmao idk',
        ]);

        DB::table('flowers')->insert([
            // le dices como se llama la columna y el valor
            'name' => 'Margarita',
            'img' => 'https://enviocoronas.com/wp-content/uploads/margaritas-principal.jpg',
            'desc' => 'La margarita (Bellis perennis), es una planta herbácea de la familia de las asteráceas (Asteraceae) muy utilizada a efectos decorativos mezclada con el césped, por sus colores y su resistencia a la siega.',
            'care' => 'Lmao idk',
        ]);

        DB::table('flowers')->insert([
            // le dices como se llama la columna y el valor
            'name' => 'Clavel',
            'img' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Gartennelke_1.jpg/240px-Gartennelke_1.jpg',
            'desc' => 'El clavel o clavelina1​ (Dianthus caryophyllus) es una planta herbácea perteneciente a la familia de las Caryophyllaceae, es considerada como flor del agua, difundida en las regiones mediterráneas2​ Es espontánea en la flora de la península ibérica. En su forma típica es una planta cespitosa,2​ con numerosos vástagos de hasta 1 m de altura. Sus hojas son lineales, angostas, opuestas y envainadoras, más anchas las basales que las caulinares. Cada tallo forma una flor terminal.2​ Sus flores son vistosas, pedunculadas en panícula o cima laxa, a veces solitarias, de bordes más o menos dentados.
                ',
            'care' => 'Lmao idk',
        ]);

        DB::table('flowers')->insert([
            // le dices como se llama la columna y el valor
            'name' => 'Trébol',
            'img' => 'https://t0.gstatic.com/licensed-image?q=tbn:ANd9GcQKkLe34tKAOm34M1Jh2KKn-CfVYf83PjJ01IZS0xSAJFBmC55C1sYSjuZdiDU5K6XO',
            'desc' => 'Trifolium es un género que comprende unas 250 especies aceptadas, de las más de 1100 descritas,3​ de plantas de la familia Fabaceae (Leguminosae), conocidas popularmente como tréboles. Se caracteriza por tener hojas que casi siempre se dividen en tres folíolos.​',
            'care' => 'Lmao idk',
        ]);

        DB::table('flowers')->insert([
            // le dices como se llama la columna y el valor
            'name' => 'Tulipan',
            'img' => 'https://picris.com/wp-content/uploads/2021/03/tulipan-rosado.jpg',
            'desc' => 'Tulipa es un género de plantas perennes y bulbosas perteneciente a la familia Liliaceae, en el que se incluyen los populares tulipanes, nombre común con el que se designa a todas las especies, híbridos y cultivares de este género. Tulipa contiene aproximadamente 150 especies e innumerables cantidades de híbridos y cultivares conseguidos a través de mejoramiento genético que los floricultores fueron realizando desde el siglo xvi.2​',
            'care' => 'Lmao idk',
        ]);

        // Seeders para insertar flores en la tabla 'flowers'
        DB::table('flowers')->insert([
            'name' => 'Girasol',
            'img' => 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTn90P6pTiPIh4RKxKkUJ6AbesakuhuERQj2JJ1Dwjz2rP4TyNv',
            'desc' => 'El girasol es una planta herbácea anual de la familia de las asteráceas. Algunas de sus variedades, como el Helianthus annuus, son muy apreciadas en jardinería por la belleza de sus flores y la facilidad de su cultivo.',
            'care' => 'Los girasoles necesitan pleno sol y suelos bien drenados. Se riegan con regularidad pero no toleran el encharcamiento. Se recomienda eliminar las malas hierbas y los brotes secundarios para favorecer el crecimiento de la planta principal.'
        ]);

        DB::table('flowers')->insert([
            'name' => 'Orquídea',
            'img' => 'https://www.massogarden.com/images/plantas/Orqu%C3%ADdea.jpg',
            'desc' => 'Las orquídeas son una de las mayores familias de plantas con flores, con alrededor de 25,000 especies reconocidas y más de 100,000 híbridos. Se encuentran en casi todos los hábitats, excepto en el agua salada y completamente congelada, desde el Ártico hasta la Antártida.',
            'care' => 'Las orquídeas necesitan luz indirecta y temperaturas moderadas. Se deben regar cuando el sustrato esté seco y se deben fertilizar durante la temporada de crecimiento. También es importante repotarlas cada pocos años.'
        ]);
    }
}
