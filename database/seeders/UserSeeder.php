<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            // le dices como se llama la columna y el valor
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'profile_image' => '/storage/profile_images/admin.png',
            'password' => 'admin'
        ]);
        DB::table('users')->insert([
            // le dices como se llama la columna y el valor
            'username' => 'test',
            'email' => 'test@test.com',
            'profile_image' => '/storage/profile_images/regular.gif',
            'password' => '1'
        ]);
    }
}
