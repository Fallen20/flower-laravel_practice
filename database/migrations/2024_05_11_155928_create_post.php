<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
      //cuando se crea, que es lo que debe de crearse
      Schema::create('post', function (Blueprint $table) {
        $table->id();
        $table->unsignedBigInteger("user_id");///esto ha de ser del mismo tipo de la foranea
        $table->foreign('user_id')->references('id')->on('users');//el creador

        $table->string('img');
        $table->text('description');
        $table->string('flower');//un tag que muestra que flor sale

        $table->timestamp('created_at')->useCurrent();
        $table->timestamp('updated_at')->useCurrent();
    });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post');
    }
};
