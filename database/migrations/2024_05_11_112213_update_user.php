<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //como ya está creado, no es create sino table (para referirse a una que ya está creada)
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('username')->unique();
            $table->string('profile_image')->nullable();


            $table->dropColumn('email_verified_at');
            $table->dropColumn('remember_token');
        });

        //user, password
        //email
        //img
        //array de posts no -tabla intermedia
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
