<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User; //adjuntamos el modelo
use App\Models\Post; //adjuntamos el modelo
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function isEmpty($dato)
    {
        return empty(trim($dato));
    }

    //crear user

    public function register_auth(Request $request)
    {

        //comprobaciones

        //EMAIL
        //vacio
        if ($this->isEmpty($request->email)) {
            Session::flash('wrongRegister', 'Email cant be empty');
            return redirect()->back();
        }

        //si existe en la DDBB
        if (User::ByEmail($request->email)->first() != null) {
            Session::flash('wrongRegister', 'Email already exists');
            return redirect()->back();
        }

        //USERNAME
        //vacio
        if ($this->isEmpty($request->username)) {
            Session::flash('wrongRegister', 'Username cant be empty');
            return redirect()->back();
        }

        //si existe en la DDBB
        if (User::ByUsername($request->username)->first() != null) {
            Session::flash('wrongRegister', 'Username already exists');
            return redirect()->back();
        }

        //PASS
        if ($this->isEmpty($request->pass_login) || $this->isEmpty($request->pass_login_Repeat)) {
            Session::flash('wrongRegister', 'Password cant be empty');
            return redirect()->back();
        }

        //passwd siendo iguales
        if ($request->pass_login != $request->pass_login_Repeat) {
            Session::flash('wrongRegister', 'Passwords arent the same');
            return redirect()->back();
        }

        //todo es correcto, crear el usuario
        $user = new User;

        //rellenar cambos
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = $request->pass_login;
        $user->profile_image = '/storage/profile_images/regular.gif';

        //guardarlo
        $user->save();

        //guardar en sesion que esta login
        session()->put('isLoged', true);

        // /guardar en sesion las credenciales. Como el email/name es unico, se puede guardar cualquiera
        session()->put('userLogged', $user->username);

        //devolver al usuario a main
        return redirect()->route('load_main');
    }

    //logout
    public function logout()
    {
        //pillar session y borrar userLogged
        session()->forget('userLogged');

        //borrar isLog
        session()->forget('isLoged');

        //devolver al usuario a donde estaba
        return redirect()->route('load_main');
    }

    //login user
    public function login_auth(Request $request)
    {

        //comprobar que no está vacio
        if ($this->isEmpty($request->email)) {
            Session::flash('wrongEmail', 'Email cant be empty');
            return redirect()->back();
        }
        if ($this->isEmpty($request->password)) {
            Session::flash('wrongEmail', 'password cant be empty');
            return redirect()->back();
        }

        //comprobar que el emaail existe
        if (User::ByEmail($request->email)->first() == null) {
            Session::flash('wrongEmail', 'Email is not registered in our DDBB');
            return redirect()->back();
        }

        //recuperar usuario (porque existe)
        $user = User::ByEmail($request->email)->first();

        //comprobar que las contras son las mismas

        if ($user->password != $request->password) {
            Session::flash('wrongEmail', 'Incorrect password');
            return redirect()->back();
        }

        //todo correcto, guardar en sesion


        //guardar en sesion que esta login
        session()->put('isLoged', true);

        // /guardar en sesion las credenciales. Como el email/name es unico, se puede guardar cualquiera
        session()->put('userLogged', $user->username);

        //devolver al usuario 
        return redirect()->route('load_main');
    }

    public function profile($username)
    {
        //recuperar usuario
        $user = User::ByUsername($username)->first();

        //recuperar los post que ha creado
        $postUser = count(Post::ByUserId($user->id)->get());

        return view('userRelated/profile', [
            'user' => $user,
            'postUser' => $postUser,
        ]);
    }

    public function editProfile($userEnvio)
    {
        //recuperar el usuario en sesion
        $userSession = session()->get('userLogged');

        //si el usuario a editar no es el mismo que esta login
        if ($userSession != $userEnvio) {
            return redirect()->back();
        }

        //recuperar usuario
        $user = User::ByUsername($userSession)->first();


        return view('userRelated/editProfile', [
            'user' => $user
        ]);
    }

    public function updateProfile(Request $request)
    {

        //recuperra usuario en sesion
        $user = User::byUsername(session()->get('userLogged'))->first();

        //mirar que hay que actualizar
        
    

        //comprobar que la foto no es la misma, (null=no se ha subido nada, nombre=se ha subido algo)
        if ($request->image != null) {
            
            $img=$request->file('image');

            //borrar la antigua si no ES la default
            if ($user->profile_image != '/storage/profile_images/regular.gif') {
                Storage::delete($user->profile_image);
            }
            
            
            

            //subir foto
            $subida = $img->storeAs('profile_images', uniqid() . "." . explode("/", $img->getMimeType())[1], 'public');
            
            $url = Storage::url($subida); //esta url es el link que se usa para mostrarla tambien


            //actualizar user
            $user->profile_image = $url;
        }

        //si se ha cambiado el nombre
        if ($request->username != $user->username && !$this->isEmpty($request->username)) {
            $user->username = $request->username;
            //se actualiza sesion
            session()->forget('userLogged');
            session()->put('userLogged', $request->username);
        }

        //email
        if ($request->email != $user->email && !$this->isEmpty($request->email)) {
            $user->email = $request->email;
            
        }



        //password
        //que ambos campos sean los mismo
        //que no se la misma que la ya puesta
        if (!$this->isEmpty($request->password_repeat) && !$this->isEmpty($request->password) && $request->password == $request->password_repeat && $request->password != $user->password) {

            $user->password = $request->password;
        }

        $user->save();



        return view('userRelated/profile', [
            'user' => $user,
            'postUser' =>  count(Post::ByUserId($user->id)->get()),
        ]);
    }
}
