<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Flower;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Session;



class FlowerController extends Controller
{

    public function isEmpty($dato)
    {
        return empty(trim($dato));
    }


    public function flower_form()
    {

        return view('flowerCreation', [
            'datos' => null,
            'error' => null
        ]);
    }
    public function load_main()
    {

        //recuperar todas las flores en orden aleatorio (inRandomOrder)
        //pillar 2 (take)
        //y pillarlo como array (get)
        $flor = Flower::inRandomOrder()->take(2)->get();

        //devolver la view y pasarle los parametros
        return view('main', [
            'floresArray' => $flor
        ]);
    }


    //
    public function send_flower($flower)
    {

        //recuperar todas las flores en orden aleatorio (inRandomOrder)
        //pillar 2 (take)
        //y pillarlo como array (get)
        $floresArray = Flower::inRandomOrder()->take(2)->get();

        //recuperar la flor por el nombre que le paso
        $flor = Flower::byName($flower)->first();

        //devolver la view y pasarle los parametros
        return view('blog/flowerBlog', [
            'flor' => $flor,
            'floresArray' => $floresArray,
        ]);
    }


    public function random_Flower()
    {
        //buscar en la base de datos el num max de elementos en la tabla flowers
        $elementsInDB = DB::table('flowers')->count();

        //random entre 1 y ese
        $random = rand(1, $elementsInDB);

        //buscar la flor con ese numero. Ese numero es el ID
        $flor = Flower::byId($random)->first();

        //devolver la view y pasarle los parametros
        return redirect(route('flower', ['flower' => $flor->name]));
    }

    public function search_flower(Request $request)
    {
        $flor = null;
        if ($request->search != null) {
            //recuperar las flores que pueden contener lo escrito
            $flor = Flower::byLikeName($request->search)->get();
        }

        //  //devolver la view y pasarle los parametros
        return view('results/result', [
            'flores' => $flor,
            'busqueda' => $request->search
        ]);
    }


    public function create_flower(Request $request)
    {

        //IMAGE CHECK
        //mirar si el VALUE imgLink está lleno
        if ($request->imgLink == null || $this->isEmpty($request->imgLink)) {
            return FlowerController::return_form_flower($request, "Imagen vacia");
        }

        $loadingImg = "https://media1.giphy.com/media/3oEjI6SIIHBdRxXI40/200w.gif?cid=6c09b9529h62rz5xtc4rqlkosmeo02imlsi7181gex4w7w66&ep=v1_gifs_search&rid=200w.gif&ct=g";
        $errorImg = "https://img.freepik.com/psd-gratis/simbolo-x-aislado_23-2150500369.jpg";
        $blankImg = "https://developers.elementor.com/docs/assets/img/elementor-placeholder-image.png";

        //img está llena, mira a ver si la img cargada no es loading (promise no ha acabado), x (error) o el placeholder
        if (
            $request->imgSrc == $loadingImg || $request->imgSrc == $errorImg || $request->imgSrc == $blankImg
        ) {
            return FlowerController::return_form_flower($request, "Imagen no válida");
        }

        //mirar que el valor de src y link sea el mismo (significa que se ha cargado y es valido). Sino volver a la vista con el error
        if ($request->imgSrc != $request->imgLink) {
            return FlowerController::return_form_flower($request, "Imagen no cargada");
        }



        //OTHER FIELD CHECKS
        if ($request->name == null || $this->isEmpty($request->name)) {
            return FlowerController::return_form_flower($request, "name vacia");
        }
        if ($request->desc == null || $this->isEmpty($request->desc)) {
            return FlowerController::return_form_flower($request, "desc vacia");
        }
        if ($request->care == null || $this->isEmpty($request->care)) {
            return FlowerController::return_form_flower($request, "care vacia");
        }

        //DATABASE CHECK
        //mirar si existe una flor con el mismo nombre
        $existsFlower = Flower::byName($request->name)->first();

        if ($existsFlower != null) {
            return FlowerController::return_form_flower($request, "Esta flor ya está registrada");
        }
        //se hade mirar tambien con minuscula y primera mayus
        $newName = ucfirst(strtolower($request->name));

        $existsFlowerLower = Flower::byName($request->name)->first();

        if ($existsFlowerLower != null) {
            return FlowerController::return_form_flower($request, "Esta flor ya está registrada");
        }

        //todo es correcto, crear un objeto
        $newFlower = new Flower;

        //metemos datos
        $newFlower->img = $request->imgSrc;
        $newFlower->name = $request->name;
        $newFlower->desc = $request->desc;
        $newFlower->care = $request->care;

        //lo creas
        $newFlower->save();

        // /almacenar las cosas en sesion porque sino sale en la url. Flash para que sea una vez solo
        Session::flash('newFlower', $newFlower);

        return redirect()->route('done_flower');
    }

    public function done_flower()
    {
        //recuperar la sesion
        $newFlower = Session::get('newFlower');

        //envias a la pag de validacion
        return view('flowerCreated', [
            'datos' => $newFlower,
        ]);
    }

    public function return_form_flower(Request $request, $error)
    {
        return view('flowerCreation', [
            'datos' => $request,
            'error' => $error
        ]);
    }
}
