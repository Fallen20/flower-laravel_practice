<?php

namespace App\Http\Controllers;


use App\Models\Flower;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;



class SocialController extends Controller
{

    public function isEmpty($dato)
    {
        return empty(trim($dato));
    }

    //
    public function load_social()
    {
        //pillar todas las flores

        $flowers = Flower::all();

        //pillar todos los posts
        $posts = Post::all();

        //pillar los usuarios correspondientes a los posts
        //recorrerlos
        foreach ($posts as $post) {
            $post->userName = User::ById($post->user_id)->first()->username;
            $post->userPhoto = User::ById($post->user_id)->first()->profile_image;
        }


        //devolver los datos a la vista
        return view('socialMain', [
            'flores' => $flowers,
            'posts' => $posts,
            'selectedFlower' => null
        ]);
    }

    public function show_post_form()
    {
        //pillar todas las flores

        $flowers = Flower::all();

        //pillar todos los blogs

        //pillar los usuarios?

        //devolver los datos a la vista
        return view('createBlog', [
            'flores' => $flowers
        ]);
    }


    public function uploadPost(Request $request)
    {
        //recuperar campos
        $imagen = $request->file('image');
        $desc = $request->desc;
        $selectedValue = $request->selectedValue_hidden;


        //comprobar que todo está lleno
        if ($this->isEmpty($desc)) {
            Session::flash('wrongUpload', 'desc cant be empty');
            return redirect()->back();
        }
        if ($this->isEmpty($selectedValue)) {
            Session::flash('wrongUpload', 'selectedValue cant be empty');
            return redirect()->back();
        }

        //comprobar si han seleccionado imagen
        if ($imagen == null) {
            Session::flash('wrongUpload', 'imagen cant be empty');
            return redirect()->back();
        }
        //lo de la extension+mime se hace en el script

        //subir foto
        $subida = $imagen->storeAs('post_images', uniqid() . "." . explode("/", $imagen->getMimeType())[1], 'public');
        $url = Storage::url($subida); //esta url es el link que se usa para mostrarla tambien

        //crear el post
        $post = new Post;

        //rellenar campos
        $post->img = $url;
        $post->description = $desc;
        $post->flower = $selectedValue;
        //recuperar el user de la sesion+recuperar el id de ese username
        $post->user_id = User::ByUsername(session()->get('userLogged'))->first()->id;

        //crearlo
        $post->save();

        //devolver al user a social
        return redirect()->route('load_social');
    }


    public function flower_filter($flower)
    {
        //recuperar todas los POST donde tag es=flower
        $posts = Post::ByFlower($flower)->get();


        //cargar la vista pero con las flores
        //pillar los usuarios correspondientes a los posts
        //recorrerlos
        foreach ($posts as $post) {
            $post->userName = User::ById($post->user_id)->first()->username;
            $post->userPhoto = User::ById($post->user_id)->first()->profile_image;
        }


        //devolver los datos a la vista
        return view('socialMain', [
            'flores' => Flower::all(),
            'posts' => $posts,
            'selectedFlower' => $flower
        ]);
    }
    public function flower_filter_dropdown(Request $request)
    {

        //comprobar que se ha seleccionado algo
        if (
            gettype($request->arraySeleccion) != 'array' ||
            count($request->arraySeleccion) == 0
        ) {
            return redirect()->route('load_social');
        }



        $posts = [];

        foreach ($request->arraySeleccion as $flor) {
            // //recuperar todas los POST donde tag es=flower
            $posts = array_merge($posts, Post::ByFlower($flor)->get()->all());
        }



        //cargar la vista pero con las flores
        //pillar los usuarios correspondientes a los posts
        //recorrerlos
        foreach ($posts as $post) {
            $post->userName = User::ById($post->user_id)->first()->username;
            $post->userPhoto = User::ById($post->user_id)->first()->profile_image;
        }


        //devolver los datos a la vista
        return view('socialMain', [
            'flores' => Flower::all(),
            'posts' => $posts,
            'selectedFlower' => $request->arraySeleccion
        ]);
    }


    public function deletePost($id){
        //recuperar post
        $post=Post::ById($id)->first();

        //borrar la foto
        Storage::delete($post->img);

        //borrar el post
        $post->delete();

   

        //pillar todos los posts
        $posts = Post::all();

        //pillar los usuarios correspondientes a los posts
        //recorrerlos
        foreach ($posts as $post) {
            $post->userName = User::ById($post->user_id)->first()->username;
            $post->userPhoto = User::ById($post->user_id)->first()->profile_image;
        }

        //devolver al blog
        return view('socialMain', [
            'flores' => Flower::all(),
            'posts' => $posts,
            'selectedFlower' => null
        ]);
    }
}
