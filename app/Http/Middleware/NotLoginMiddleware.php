<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NotLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if ($request->session()->has('isLoged')) {
            $user = $request->session()->get('isLoged');


            if (!$user) { //no ha iniciadio sesion
                return redirect(route('login'));
            } else {
                return $next($request);
            }
        } else {
            return redirect(route('login'));
        }
    }
}
