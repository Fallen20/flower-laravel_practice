<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LoginMiddleware
{
    public function handle(Request $request, Closure $next)
    {

        if ($request->session()->has('isLoged')) {
            $user = $request->session()->get('isLoged');


            if ($user) { //ha iniciadio sesion
                return $next($request);
            } else {
                return redirect(route('login'));
            }
        } else {
            return $next($request);
        }
    }
}
