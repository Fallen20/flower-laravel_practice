<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'users';

    //recuperar flor por nombre
    public function scopeByUsername($query, $username)
    {
        return $query->where('username', $username);
    }

    public function scopeByEmail($query, $email)
    {
        return $query->where('email', 'LIKE', '%' . $email . '%');
    }
    public function scopeByPassword($query, $password)
    {
        return $query->where('password', 'LIKE', '%' . $password . '%');
    }

    //recuperar flor por ID
    public function scopeById($query, $id)
    {
        return $query->where('id', $id);
    }
}
