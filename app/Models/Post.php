<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    //tabla
    protected $table = 'post';

    public function scopeByFlower($query, $flower)
    {
        return $query->where('flower', $flower);
    }
    public function scopeById($query, $id)
    {
        return $query->where('id', $id);
    }

    public function scopeByUserId($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
}
