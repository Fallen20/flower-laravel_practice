<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flower extends Model
{
    use HasFactory;
    //tabla
    protected $table = 'flowers';



    //recuperar flor por nombre
    public function scopeByName($query, $name)
    {
        return $query->where('name', $name);
    }
    public function scopeByLikeName($query, $name)
    {
        return $query->where('name', 'LIKE', '%' . $name . '%');
    }

    //recuperar flor por ID
    public function scopeById($query, $id)
    {
        return $query->where('id', $id);
    }
}
